import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.naming.spi.DirStateFactory.Result;
import javax.swing.BoxLayout;
import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.JTextField;
import java.awt.GridLayout;
import javax.swing.JButton;
import java.awt.FlowLayout;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.CardLayout;
import javax.swing.SwingConstants;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.border.EtchedBorder;
import javax.swing.UIManager;

public class Calculadora {

	private JFrame frmCalculadora;
	private JTextField mostrar;
	private char op =' ';

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Calculadora window = new Calculadora();
					window.frmCalculadora.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Calculadora() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmCalculadora = new JFrame();
		frmCalculadora.setResizable(false);
		frmCalculadora.setTitle("Calculadora");
		frmCalculadora.setBounds(100, 100, 575, 656);
		frmCalculadora.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmCalculadora.getContentPane().setLayout(new BorderLayout(0, 0));

		JPanel sur = new JPanel();
		frmCalculadora.getContentPane().add(sur, BorderLayout.NORTH);
		sur.setLayout(new GridLayout(1, 1, 0, 0));

		mostrar = new JTextField();
		mostrar.setEditable(false);
		mostrar.setFont(new Font("Sylfaen", Font.PLAIN, 34));
		sur.add(mostrar);
		mostrar.setColumns(10);

		JPanel centro = new JPanel();
		frmCalculadora.getContentPane().add(centro, BorderLayout.CENTER);
		centro.setLayout(new GridLayout(4, 4, 1, 1));

		JButton siete = new JButton("7");
		siete.setBackground(new Color(240, 255, 255));
		siete.setBorder(UIManager.getBorder("CheckBox.border"));
		siete.setFocusPainted(false);
		siete.setFont(new Font("Sylfaen", Font.PLAIN, 23));
		centro.add(siete);

		JButton ocho = new JButton("8");
		ocho.setBackground(new Color(240, 255, 255));
		ocho.setBorder(UIManager.getBorder("CheckBox.border"));
		ocho.setFocusPainted(false);
		ocho.setFont(new Font("Sylfaen", Font.PLAIN, 23));
		centro.add(ocho);

		JButton nueve = new JButton("9");
		nueve.setBackground(new Color(240, 255, 255));
		nueve.setBorder(UIManager.getBorder("CheckBox.border"));
		nueve.setFocusPainted(false);
		nueve.setFont(new Font("Sylfaen", Font.PLAIN, 23));
		centro.add(nueve);

		JButton Suma = new JButton("+");
		Suma.setBackground(Color.DARK_GRAY);
		Suma.setBorder(UIManager.getBorder("Tree.editorBorder"));
		Suma.setForeground(new Color(255, 255, 255));
		Suma.setFocusPainted(false);
		Suma.setFont(new Font("Sylfaen", Font.PLAIN, 23));
		centro.add(Suma);

		JButton cuatro = new JButton("4");
		cuatro.setBackground(new Color(240, 255, 255));
		cuatro.setBorder(UIManager.getBorder("CheckBox.border"));
		cuatro.setFocusPainted(false);
		cuatro.setFont(new Font("Sylfaen", Font.PLAIN, 23));
		centro.add(cuatro);

		JButton cinco = new JButton("5");
		cinco.setBackground(new Color(240, 255, 255));
		cinco.setBorder(UIManager.getBorder("CheckBox.border"));
		cinco.setFocusPainted(false);
		cinco.setFont(new Font("Sylfaen", Font.PLAIN, 23));
		centro.add(cinco);

		JButton seis = new JButton("6");
		seis.setBackground(new Color(240, 255, 255));
		seis.setBorder(UIManager.getBorder("CheckBox.border"));
		seis.setFocusPainted(false);
		seis.setFont(new Font("Sylfaen", Font.PLAIN, 23));
		centro.add(seis);

		JButton Resta = new JButton("-");
		Resta.setBackground(Color.DARK_GRAY);
		Resta.setBorder(UIManager.getBorder("Tree.editorBorder"));
		Resta.setForeground(new Color(255, 255, 255));
		Resta.setFocusPainted(false);
		Resta.setFont(new Font("Sylfaen", Font.PLAIN, 23));
		centro.add(Resta);

		JButton uno = new JButton("1");
		uno.setBackground(new Color(240, 255, 255));
		uno.setBorder(UIManager.getBorder("CheckBox.border"));
		uno.setFocusPainted(false);
		uno.setFont(new Font("Sylfaen", Font.PLAIN, 23));
		centro.add(uno);

		JButton dos = new JButton("2");
		dos.setBackground(new Color(240, 255, 255));
		dos.setBorder(UIManager.getBorder("CheckBox.border"));
		dos.setFocusPainted(false);
		dos.setFont(new Font("Sylfaen", Font.PLAIN, 23));
		centro.add(dos);

		JButton tres = new JButton("3");
		tres.setBackground(new Color(240, 255, 255));
		tres.setBorder(UIManager.getBorder("CheckBox.border"));
		tres.setFocusPainted(false);
		tres.setFont(new Font("Sylfaen", Font.PLAIN, 23));
		centro.add(tres);

		JButton Multiplicacion = new JButton("X");
		Multiplicacion.setBackground(Color.DARK_GRAY);
		Multiplicacion.setBorder(UIManager.getBorder("Tree.editorBorder"));
		Multiplicacion.setForeground(new Color(255, 255, 255));
		Multiplicacion.setFocusPainted(false);
		Multiplicacion.setFont(new Font("Sylfaen", Font.PLAIN, 23));
		centro.add(Multiplicacion);

		JButton punto = new JButton(".");
		punto.setForeground(new Color(255, 255, 255));
		punto.setBackground(Color.DARK_GRAY);
		punto.setBorder(UIManager.getBorder("Tree.editorBorder"));
		punto.setFocusPainted(false);
		punto.setFont(new Font("Sylfaen", Font.PLAIN, 23));
		centro.add(punto);

		JButton cero = new JButton("0");
		cero.setBackground(new Color(240, 255, 255));
		cero.setBorder(UIManager.getBorder("CheckBox.border"));
		cero.setFocusPainted(false);
		cero.setFont(new Font("Sylfaen", Font.PLAIN, 23));
		centro.add(cero);

		JButton Resultado = new JButton("=");
		Resultado.setBackground(Color.DARK_GRAY);
		Resultado.setBorder(UIManager.getBorder("Tree.editorBorder"));
		Resultado.setForeground(new Color(255, 255, 255));
		Resultado.setFocusPainted(false);
		Resultado.setFont(new Font("Sylfaen", Font.PLAIN, 23));
		centro.add(Resultado);

		JButton Division = new JButton("/");
		Division.setBackground(Color.DARK_GRAY);
		Division.setBorder(UIManager.getBorder("Tree.editorBorder"));
		Division.setForeground(new Color(255, 255, 255));
		Division.setFocusPainted(false);
		Division.setFont(new Font("Sylfaen", Font.PLAIN, 23));
		centro.add(Division);

		JPanel abajo = new JPanel();
		frmCalculadora.getContentPane().add(abajo, BorderLayout.SOUTH);
		abajo.setLayout(new GridLayout(1, 1, 0, 0));

		JButton borrar = new JButton("Borrar");
		borrar.setBackground(Color.DARK_GRAY);
		borrar.setBorder(UIManager.getBorder("Tree.editorBorder"));
		borrar.setFocusPainted(false);
		borrar.setForeground(Color.RED);
		borrar.setFont(new Font("Sylfaen", Font.PLAIN, 23));
		abajo.add(borrar);

		/* Eventos */
		cero.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mostrar.setText(mostrar.getText() + "0");
			}
		});
		uno.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mostrar.setText(mostrar.getText() + "1");
			}
		});
		dos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mostrar.setText(mostrar.getText() + "2");
			}
		});
		tres.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mostrar.setText(mostrar.getText() + "3");
			}
		});
		cuatro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mostrar.setText(mostrar.getText() + "4");
			}
		});
		cinco.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mostrar.setText(mostrar.getText() + "5");
			}
		});
		seis.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mostrar.setText(mostrar.getText() + "6");
			}
		});
		siete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mostrar.setText(mostrar.getText() + "7");
			}
		});
		ocho.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mostrar.setText(mostrar.getText() + "8");
			}
		});
		nueve.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mostrar.setText(mostrar.getText() + "9");
			}
		});
		Suma.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				op = '+';
				mostrar.setText(mostrar.getText() + "+");
				Suma.setEnabled(false);
				Resta.setEnabled(false);
				Multiplicacion.setEnabled(false);
				Division.setEnabled(false);
				punto.setEnabled(true);
			}
		});
		Resta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				op = '-';
				mostrar.setText(mostrar.getText() + "-");
				Suma.setEnabled(false);
				Resta.setEnabled(false);
				Multiplicacion.setEnabled(false);
				Division.setEnabled(false);
				punto.setEnabled(true);
			}
		});
		Multiplicacion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				op = 'X';
				mostrar.setText(mostrar.getText() + "X");
				Suma.setEnabled(false);
				Resta.setEnabled(false);
				Multiplicacion.setEnabled(false);
				Division.setEnabled(false);
				punto.setEnabled(true);
			}
		});
		Division.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				op = '/';
				mostrar.setText(mostrar.getText() + "/");
				Suma.setEnabled(false);
				Resta.setEnabled(false);
				Multiplicacion.setEnabled(false);
				Division.setEnabled(false);
				punto.setEnabled(true);
			}
		});
		punto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mostrar.setText(mostrar.getText() + ".");
				punto.setEnabled(false);
			}
		});
		borrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mostrar.setText("");
				Suma.setEnabled(true);
				Resta.setEnabled(true);
				Multiplicacion.setEnabled(true);
				Division.setEnabled(true);
				punto.setEnabled(true);
			}
		});
		Resultado.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int posicion;
				String num1;
				String num2;
				double total;
				switch (op) {
				case '+'://10+20
					posicion = mostrar.getText().indexOf('+'); 										//saber en que posicion esta el +        10 [+] 10
					num1 = mostrar.getText().substring(0, posicion);								//una vez dividido el texto que coja todo desde la posicion 0 hasta posicion(+) 10
					num2 = mostrar.getText().substring(posicion+1, mostrar.getText().length());	// nos coje la otra parte despues del + hasta el final 20
					total = Double.parseDouble(num1) + Double.parseDouble(num2);
					mostrar.setText(Double.toString(total));
					break;
				case '-':
					posicion = mostrar.getText().indexOf('-'); 										//saber en que posicion esta el +        10 [+] 10
					num1 = mostrar.getText().substring(0, posicion);								//una vez dividido el texto que coja todo desde la posicion 0 hasta posicion(+) 10
					num2 = mostrar.getText().substring(posicion+1, mostrar.getText().length());	// nos coje la otra parte despues del + hasta el final 20
					total = Double.parseDouble(num1) - Double.parseDouble(num2);
					mostrar.setText(Double.toString(total));
					break;
				case 'X':
					posicion = mostrar.getText().indexOf('X'); 										//saber en que posicion esta el +        10 [+] 10
					num1 = mostrar.getText().substring(0, posicion);								//una vez dividido el texto que coja todo desde la posicion 0 hasta posicion(+) 10
					num2 = mostrar.getText().substring(posicion+1, mostrar.getText().length());	// nos coje la otra parte despues del + hasta el final 20
					total = Double.parseDouble(num1) * Double.parseDouble(num2);
					mostrar.setText(Double.toString(total));
					break;
				case '/':
					posicion = mostrar.getText().indexOf('/'); 										//saber en que posicion esta el +        10 [+] 10
					num1 = mostrar.getText().substring(0, posicion);								//una vez dividido el texto que coja todo desde la posicion 0 hasta posicion(+) 10
					num2 = mostrar.getText().substring(posicion+1, mostrar.getText().length());	// nos coje la otra parte despues del + hasta el final 20
					total = Double.parseDouble(num1) / Double.parseDouble(num2);
					mostrar.setText(Double.toString(total));
					break;
				}
			}
		});
	}
}
