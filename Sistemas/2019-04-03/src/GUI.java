import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import java.awt.Color;
import java.awt.SystemColor;
import java.net.MalformedURLException;
import java.net.URL;

import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import java.awt.Cursor;
import java.awt.Component;
import java.awt.Font;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import javax.swing.JPasswordField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.AbstractListModel;
import javax.swing.ListSelectionModel;
import java.awt.Toolkit;
import com.toedter.calendar.JCalendar;
import com.toedter.calendar.JDateChooser;
import javax.swing.border.LineBorder;
import javax.swing.border.BevelBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.JDesktopPane;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

public class GUI {

	// Representa a la ventana en su totalidad
	private JFrame frmEncuesta;
	private JTextField usuario;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private final ButtonGroup buttonGroup_1 = new ButtonGroup();
	private JPasswordField contrasenia;
	private final ButtonGroup buttonGroup_2 = new ButtonGroup();
	private final ButtonGroup buttonGroup_3 = new ButtonGroup();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI window = new GUI();
					window.frmEncuesta.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * 
	 * @throws MalformedURLException
	 */
	public GUI() throws MalformedURLException {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 * 
	 */
	private void initialize() throws MalformedURLException {
		frmEncuesta = new JFrame();
		frmEncuesta.setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\CFGS\\Downloads\\award_icon-icons.com_66110.png"));
		frmEncuesta.setResizable(false);
		frmEncuesta.setTitle("Encuesta");
		frmEncuesta.setBounds(100, 100, 786, 566); // X ,Y Ancho, Largo
		frmEncuesta.setLocationRelativeTo(null);
		frmEncuesta.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmEncuesta.getContentPane().setLayout(null);
		
		//Campo de texto vacio para introducir valores
		usuario = new JTextField();
		usuario.setToolTipText("Introduzca su nombre"); //Mensaje que nos muestra que rellenar en el campo vacio.
		usuario.setBounds(181, 21, 294, 22);
		frmEncuesta.getContentPane().add(usuario);
		usuario.setColumns(5);
		
		JLabel lblIntroduzcaSuNombre = new JLabel("Introduzca su contrase\u00F1a"); //Texto, nos servira para poner textos en las ventanas
		lblIntroduzcaSuNombre.setHorizontalAlignment(SwingConstants.CENTER);
		lblIntroduzcaSuNombre.setBounds(20, 52, 123, 18);
		frmEncuesta.getContentPane().add(lblIntroduzcaSuNombre);
		
		JRadioButton profesor = new JRadioButton("Profesor");
		profesor.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		buttonGroup.add(profesor);
		profesor.setBounds(20, 85, 109, 23);
		frmEncuesta.getContentPane().add(profesor);
		
		JRadioButton alumno = new JRadioButton("Alumno");
		alumno.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		buttonGroup.add(alumno);
		alumno.setBounds(20, 111, 109, 23);
		frmEncuesta.getContentPane().add(alumno);
		
		JRadioButton hombre = new JRadioButton("Hombre");
		hombre.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		buttonGroup_1.add(hombre);
		hombre.setBounds(131, 85, 109, 23);
		frmEncuesta.getContentPane().add(hombre);
		
		JRadioButton mujer = new JRadioButton("Mujer");
		mujer.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		buttonGroup_1.add(mujer);
		mujer.setBounds(131, 111, 109, 23);
		frmEncuesta.getContentPane().add(mujer);
		
		JLabel lblSeleccioneCiudad = new JLabel("Aficiones");
		lblSeleccioneCiudad.setHorizontalAlignment(SwingConstants.LEFT);
		lblSeleccioneCiudad.setBounds(193, 155, 123, 18);
		frmEncuesta.getContentPane().add(lblSeleccioneCiudad);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		comboBox.setMaximumRowCount(5);
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"MADRID", "BARCELONA", "VALENCIA", "SEVILLA", "BILBAO", "ZARAGOZA", "ALBACETE"}));
		comboBox.setSelectedIndex(1);
		comboBox.setBounds(369, 84, 109, 20);
		frmEncuesta.getContentPane().add(comboBox);
		
		JLabel foto = new JLabel("");
		foto.setHorizontalAlignment(SwingConstants.CENTER);
		foto.setBounds(476, 23, 294, 219);
		frmEncuesta.getContentPane().add(foto);
		
		JLabel lblObservaciones = new JLabel("Observaciones");
		lblObservaciones.setBounds(20, 157, 115, 14);
		frmEncuesta.getContentPane().add(lblObservaciones);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(20, 182, 163, 213);
		frmEncuesta.getContentPane().add(scrollPane);
		
		JTextArea observaciones = new JTextArea();
		scrollPane.setViewportView(observaciones);
		observaciones.setLineWrap(true);
		
		JLabel label_1 = new JLabel("Introduzca su nombre");
		label_1.setHorizontalAlignment(SwingConstants.CENTER);
		label_1.setBounds(10, 23, 123, 18);
		frmEncuesta.getContentPane().add(label_1);
		
		contrasenia = new JPasswordField();
		contrasenia.setEnabled(false);
		contrasenia.setToolTipText("Contrase\u00F1a");
		contrasenia.setBounds(181, 54, 294, 20);
		frmEncuesta.getContentPane().add(contrasenia);
		
		JButton boton = new JButton("Aceptar");
		boton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		boton.setEnabled(false);
		buttonGroup_3.add(boton);
		boton.setBounds(659, 503, 89, 23);
		frmEncuesta.getContentPane().add(boton);
		
		JCheckBox acepto = new JCheckBox("Acepto los t\u00E9rminos");
		acepto.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		
		acepto.setBounds(626, 472, 144, 23);
		frmEncuesta.getContentPane().add(acepto);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		scrollPane_1.setBounds(193, 184, 163, 121);
		frmEncuesta.getContentPane().add(scrollPane_1);
		
		JList list = new JList();
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list.setToolTipText("Seleccione uno");
		list.setSelectionForeground(Color.BLACK);
		list.setSelectionBackground(new Color(153, 255, 0));
		scrollPane_1.setViewportView(list);
		list.setFont(new Font("Arial", Font.ITALIC, 15));
		list.setModel(new AbstractListModel() {
			String[] values = new String[] {"Videojuegos", "Musica", "Cine", "Deportes", "Viajar", "Cocinar", "Salir de fiesta", "Ir de compras", "Programaci\u00F3n"};
			public int getSize() {
				return values.length;
			}
			public Object getElementAt(int index) {
				return values[index];
			}
		});
		list.setSelectedIndex(4);
		
		JLabel label_2 = new JLabel("Seleccione ciudad");
		label_2.setHorizontalAlignment(SwingConstants.LEFT);
		label_2.setBounds(246, 85, 123, 18);
		frmEncuesta.getContentPane().add(label_2);
		
		JDateChooser dateChooser = new JDateChooser();
		dateChooser.getCalendarButton().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		dateChooser.setDateFormatString("dd-MM-yy");
		dateChooser.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		dateChooser.setBorder(new EmptyBorder(0, 0, 0, 0));
		dateChooser.setBounds(368, 114, 109, 20);
		frmEncuesta.getContentPane().add(dateChooser);
		
		JLabel lblFechaDeNacimiento = new JLabel(" Fecha de nacimiento");
		lblFechaDeNacimiento.setHorizontalAlignment(SwingConstants.LEFT);
		lblFechaDeNacimiento.setBounds(246, 115, 113, 14);
		frmEncuesta.getContentPane().add(lblFechaDeNacimiento);
		
		
		//eventos.....................................
				
		usuario.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				contrasenia.setEnabled(true);
				
			}
		});
		
		contrasenia.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				if (contrasenia.getText().isEmpty()) {
					acepto.setSelected(true);
					boton.setEnabled(true);
				}
				else {
					acepto.setSelected(false);
					boton.setEnabled(false);
				}
			}
		});
		frmEncuesta.addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent arg0) {
				profesor.setSelected(true);
				mujer.setSelected(true);
			}
		});
		alumno.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int respuesta = JOptionPane.showConfirmDialog(
								frmEncuesta, 
								"�Realmente eres un alumno?",
								"Confirmar por favor",
								JOptionPane.YES_NO_CANCEL_OPTION,
								JOptionPane.QUESTION_MESSAGE );
				if(respuesta==0) {
					observaciones.setText("Eres alumno");
				}
				if (respuesta==1){
					observaciones.setText("No eres alumno");
					alumno.setSelected(false);
					profesor.setSelected(true);
				}
				if(respuesta==2) {
					observaciones.setText("Haz cancelado la accion");
				}
			}
			
		});
		acepto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(acepto.isSelected()) {
					boton.setEnabled(true);
				}
				else {
					boton.setEnabled(false);
				}
			}
		});
		comboBox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				if(comboBox.getSelectedIndex()==0) {
					foto.setIcon(new ImageIcon("IMG\\madrid.jpg"));
				}
				if(comboBox.getSelectedIndex()==1) {
					foto.setIcon(new ImageIcon("IMG\\barcelona.jpg"));
				}
			}
		});
		boton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(!usuario.getText().isEmpty() && !contrasenia.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, "Hola "+ usuario.getText());
				}
				else {
					JOptionPane.showMessageDialog(null, "Faltan datos");
				}
			}
		});
	}
}
