
public class Principal {

    public static void main(String[] args) {
        // TODO Auto-generated method stub
        /*
         * soy
         * un comentario
         * de varias lineas
         */

        //Declaración de variables
        //variable entera
        int edad=21;
        //variable decimal
        double altura = 1.65;
        //variable caracter
        char estadoCivil = 's';
        //variable cadenas
        String nombre = "Juan Diego Fernandez";
        //variable booleana
        boolean espaniol = false;

        //Sysout control + espacio y autocompleta
        System.out.println(edad);
        System.out.println(altura);
        System.out.println("Tu edad es: "+edad+" años y mides "+altura+" CM ");

        //i tiene 21 y mides 1.65, ere un juandy

        if(edad == 21 && altura == 1.65) {
            System.out.println("Eres un juandy");
        }
        else {
            System.out.println("Eres otro");
        }

        //Si tienes más de 21 o 21 eres un viejo, sino puedes seguir estudiando

        if (edad >=21) {
            System.out.println("Eres un viejo");
        }
        else {
            System.out.println("Todavia puedes estudiar");
        }

        //Si mides mas de 1.70 o no eres de madrid, entras gratis

        if(altura > 1.70 || espaniol == false) {
            System.out.println("Entro gratis");
        }
        else {
            System.out.println("A pagar");
        }

        //Si  estas casado
        //si mides mas o igual a 1.75
        //si no eres de madrid... Regalo viaje a Punta Cana

        if(estadoCivil == 's') {
            if(altura > 1.75) {
                if(espaniol == false) {
                    System.out.println("Viaje a Punta Cana gratis");
                }
                else {
                    System.out.println("Lástima de haber nacido en Parla");
                }
            }else {
                System.out.println("Por 10 Cm");
            }
        }else {
            System.out.println("Ella dijo no");
        }

    }

}
