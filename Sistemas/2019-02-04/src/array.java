
public class array {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
/**********************************************************************************************************************************************/		
		int [] conjunto= {8, 3, 5, 3, 17, 22, 0, 1, 9}; //array de enteros
		//				  0  1  2  3  4   5   6  7  8  
		//Esta formado por 9 numeros
		System.out.println(conjunto[4]);			//muestra el numero que esta guardado en la array, posicion 4
		System.out.println(conjunto.length);		//muestra la ultima posision de la array
		System.out.println("El array esta fomrado por "+conjunto.length+" numeros"); //uestra los numeros en total guardados en la array
		System.out.println(conjunto[0]);
		System.out.println(conjunto[8]);
		System.out.println(conjunto[conjunto.length-1]); // vemos el total de numeros de  la array
		System.out.println("\n");
		
		for(int i=0; i<conjunto.length; i++) {
			System.out.println(conjunto[i]);
		}
/**********************************************************************************************************************************************/		
		System.out.println("\n");
		char [] letras  = {'g', 't', 'o', 'w', '@', '5', '%', '$', 'F'}; // array de caracteres
		
		for(int i=0; i<letras.length; i++) {
			System.out.println(letras[i]);
		}
		
/**********************************************************************************************************************************************/		
		System.out.println("\n");
		String [] palabras = {"casa", "PC", "peRro", "teLE", "sol", "AudiA3"}; //array de cadenas de caracteres
		palabras[2]= "Gato";
		for(int i=0; i<palabras.length; i++) {
			System.out.print(palabras[i]+"\t");
		}
/**********************************************************************************************************************************************/		
		System.out.println("\n");
		double [] numeros = {34.7, 2.6, 9.5, 23.0, 12, 67.8, 127.43}; //array de numeros con deciamles y enteros
		
		for(int i=numeros.length-1; i>=0; i--) {
			System.out.print(numeros[i]+"\t");
		}

	}

}
