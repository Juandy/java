import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.Color;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.awt.event.ActionEvent;
import javax.swing.JButton;
import javax.swing.ImageIcon;

public class Areas {

	private JFrame frmAreas;
	private JTextField lado;
	private JTextField altura;
	private JTextField radio;
	private JTextField dmayor;
	private JTextField dmenor;
	private JTextField base;
	private JTextField bmenor;
	private JTextField bmayor;
	private JTextField area;
	private char op = ' ';
	DecimalFormat df = new DecimalFormat("#.00");
	Image img = new ImageIcon("IMG\\logo.png").getImage();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Areas window = new Areas();
					window.frmAreas.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Areas() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmAreas = new JFrame();
		frmAreas.getContentPane().setEnabled(false);
		frmAreas.setResizable(false);
		frmAreas.getContentPane().setBackground(Color.WHITE);
		frmAreas.setTitle("Areas");
		frmAreas.setBounds(100, 100, 559, 575);
		frmAreas.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmAreas.getContentPane().setLayout(null);
		frmAreas.setLocationRelativeTo(null);
		frmAreas.setIconImage(Toolkit.getDefaultToolkit().getImage("IMG\\logo.png"));

		JLabel lblNewLabel = new JLabel("Calculo de Areas");
		lblNewLabel.setFont(new Font("Trebuchet MS", Font.PLAIN, 20));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(75, 11, 375, 52);
		frmAreas.getContentPane().add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("Seleccione una figura");
		lblNewLabel_1.setFont(new Font("Trebuchet MS", Font.PLAIN, 13));
		lblNewLabel_1.setBounds(10, 60, 160, 31);
		frmAreas.getContentPane().add(lblNewLabel_1);

		JComboBox figuras = new JComboBox();
		figuras.setModel(new DefaultComboBoxModel(
				new String[] { "Circulo", "Cuadrado", "Rectangulo", "Rombo", "Trapecio", "Triangulo" }));
		figuras.setBounds(10, 102, 160, 20);
		frmAreas.getContentPane().add(figuras);

		JLabel Lado = new JLabel("Lado");
		Lado.setFont(new Font("Verdana", Font.BOLD, 13));
		Lado.setBounds(220, 108, 93, 20);
		frmAreas.getContentPane().add(Lado);

		JLabel Altura = new JLabel("Altura");
		Altura.setFont(new Font("Verdana", Font.BOLD, 13));
		Altura.setBounds(220, 139, 93, 20);
		frmAreas.getContentPane().add(Altura);

		JLabel linea = new JLabel("");
		linea.setBorder(new LineBorder(new Color(0, 0, 0)));
		linea.setBounds(10, 49, 533, 2);
		frmAreas.getContentPane().add(linea);

		JLabel Radio = new JLabel("Radio");
		Radio.setFont(new Font("Verdana", Font.BOLD, 13));
		Radio.setBounds(220, 170, 93, 20);
		frmAreas.getContentPane().add(Radio);

		JLabel dMayor = new JLabel("Diagonal mayor");
		dMayor.setFont(new Font("Verdana", Font.BOLD, 13));
		dMayor.setBounds(220, 201, 122, 20);
		frmAreas.getContentPane().add(dMayor);

		JLabel dMenor = new JLabel("Diagonal Menor");
		dMenor.setFont(new Font("Verdana", Font.BOLD, 13));
		dMenor.setBounds(220, 232, 122, 20);
		frmAreas.getContentPane().add(dMenor);

		JLabel bMenor = new JLabel("Base Menor");
		bMenor.setFont(new Font("Verdana", Font.BOLD, 13));
		bMenor.setBounds(220, 293, 122, 20);
		frmAreas.getContentPane().add(bMenor);

		JLabel bMayor = new JLabel("Base Mayor");
		bMayor.setFont(new Font("Verdana", Font.BOLD, 13));
		bMayor.setBounds(220, 324, 122, 20);
		frmAreas.getContentPane().add(bMayor);

		JLabel Base = new JLabel("Base");
		Base.setFont(new Font("Verdana", Font.BOLD, 13));
		Base.setBounds(220, 263, 122, 20);
		frmAreas.getContentPane().add(Base);

		lado = new JTextField();
		lado.setEnabled(false);
		lado.setBounds(398, 110, 86, 20);
		frmAreas.getContentPane().add(lado);
		lado.setColumns(10);

		altura = new JTextField();
		altura.setEnabled(false);
		altura.setColumns(10);
		altura.setBounds(398, 141, 86, 20);
		frmAreas.getContentPane().add(altura);

		radio = new JTextField();
		radio.setEnabled(false);
		radio.setColumns(10);
		radio.setBounds(398, 172, 86, 20);
		frmAreas.getContentPane().add(radio);

		dmayor = new JTextField();
		dmayor.setEnabled(false);
		dmayor.setColumns(10);
		dmayor.setBounds(398, 203, 86, 20);
		frmAreas.getContentPane().add(dmayor);

		dmenor = new JTextField();
		dmenor.setEnabled(false);
		dmenor.setColumns(10);
		dmenor.setBounds(398, 234, 86, 20);
		frmAreas.getContentPane().add(dmenor);

		base = new JTextField();
		base.setEnabled(false);
		base.setColumns(10);
		base.setBounds(398, 265, 86, 20);
		frmAreas.getContentPane().add(base);

		bmenor = new JTextField();
		bmenor.setEnabled(false);
		bmenor.setColumns(10);
		bmenor.setBounds(398, 295, 86, 20);
		frmAreas.getContentPane().add(bmenor);

		bmayor = new JTextField();
		bmayor.setEnabled(false);
		bmayor.setColumns(10);
		bmayor.setBounds(398, 326, 86, 20);
		frmAreas.getContentPane().add(bmayor);

		JLabel Area = new JLabel("Area");
		Area.setFont(new Font("Verdana", Font.BOLD, 13));
		Area.setBounds(220, 383, 122, 20);
		frmAreas.getContentPane().add(Area);

		area = new JTextField();
		area.setEditable(false);
		area.setColumns(10);
		area.setBounds(398, 383, 86, 20);
		frmAreas.getContentPane().add(area);

		JLabel label = new JLabel("");
		label.setBorder(new LineBorder(new Color(0, 0, 0)));
		label.setBounds(10, 371, 533, 2);
		frmAreas.getContentPane().add(label);

		JButton calculo = new JButton("Calcular");
		calculo.setBounds(10, 384, 89, 23);
		frmAreas.getContentPane().add(calculo);

		JLabel imagen = new JLabel("");
		imagen.setHorizontalAlignment(SwingConstants.CENTER);
		imagen.setIcon(new ImageIcon(img.getScaledInstance(193, 192, Image.SCALE_SMOOTH)));
		imagen.setBounds(10, 145, 193, 192);
		frmAreas.getContentPane().add(imagen);

		JButton salir = new JButton("Salir");
		salir.setBounds(10, 418, 89, 23);
		frmAreas.getContentPane().add(salir);

		// eventos
		figuras.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (figuras.getSelectedIndex() == 0) {
					op = '1';
					limpiarLabels();
					radio.setEnabled(true);
					radio.setBorder(new LineBorder(Color.RED));
					altura.setEnabled(false);
					base.setEnabled(false);
					bmayor.setEnabled(false);
					bmenor.setEnabled(false);
					base.setEnabled(false);
					lado.setEnabled(false);
					dmayor.setEnabled(false);
					dmenor.setEnabled(false);
				}
				if (figuras.getSelectedIndex() == 1) {
					op = '2';
					limpiarLabels();
					lado.setEnabled(true);
					lado.setBorder(new LineBorder(Color.RED));
					altura.setEnabled(false);
					base.setEnabled(false);
					bmayor.setEnabled(false);
					bmenor.setEnabled(false);
					base.setEnabled(false);
					radio.setEnabled(false);
					dmayor.setEnabled(false);
					dmenor.setEnabled(false);
				}
				if (figuras.getSelectedIndex() == 2) {
					op = '3';
					limpiarLabels();
					altura.setEnabled(true);
					base.setEnabled(true);
					base.setBorder(new LineBorder(Color.RED));
					altura.setBorder(new LineBorder(Color.RED));
					radio.setEnabled(false);
					bmayor.setEnabled(false);
					bmenor.setEnabled(false);
					lado.setEnabled(false);
					dmayor.setEnabled(false);
					dmenor.setEnabled(false);
				}
				if (figuras.getSelectedIndex() == 3) {
					op = '4';
					limpiarLabels();
					dmayor.setEnabled(true);
					dmenor.setEnabled(true);
					dmayor.setBorder(new LineBorder(Color.RED));
					dmenor.setBorder(new LineBorder(Color.RED));
					radio.setEnabled(false);
					altura.setEnabled(false);
					base.setEnabled(false);
					bmayor.setEnabled(false);
					bmenor.setEnabled(false);
					base.setEnabled(false);
					lado.setEnabled(false);
				}
				if (figuras.getSelectedIndex() == 4) {
					op = '5';
					limpiarLabels();
					altura.setEnabled(true);
					bmayor.setEnabled(true);
					bmenor.setEnabled(true);
					altura.setBorder(new LineBorder(Color.RED));
					bmayor.setBorder(new LineBorder(Color.RED));
					bmenor.setBorder(new LineBorder(Color.RED));
					radio.setEnabled(false);
					base.setEnabled(false);
					lado.setEnabled(false);
					dmayor.setEnabled(false);
					dmenor.setEnabled(false);
				}
				if (figuras.getSelectedIndex() == 5) {
					op = '6';
					limpiarLabels();
					altura.setEnabled(true);
					base.setEnabled(true);
					base.setBorder(new LineBorder(Color.RED));
					altura.setBorder(new LineBorder(Color.RED));
					radio.setEnabled(false);
					bmayor.setEnabled(false);
					bmenor.setEnabled(false);
					lado.setEnabled(false);
					dmayor.setEnabled(false);
					dmenor.setEnabled(false);
				}

			}
		});

		calculo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Double num1;
				Double num2;
				Double num3;
				double total;

				switch (op) {
				case '1':
					num1 = Double.valueOf(radio.getText());
					total = (num1 * num1) * Math.PI;
					area.setText(df.format(total));
					break;
				case '2':
					num1 = Double.valueOf(lado.getText());
					total = Math.pow(num1, 2);
					area.setText(df.format(total));
					break;
				case '3':
					num1 = Double.valueOf(base.getText());
					num2 = Double.valueOf(altura.getText());
					total = (num1 * num2);
					area.setText(df.format(total));
					break;
				case '4':
					num1 = Double.valueOf(dmayor.getText());
					num2 = Double.valueOf(dmenor.getText());
					total = (num1 * num2) / 2;
					area.setText(df.format(total));
					break;
				case '5':
					num1 = Double.valueOf(altura.getText());
					num2 = Double.valueOf(bmayor.getText());
					num3 = Double.valueOf(bmenor.getText());
					total = (num1 * ((num2 + num3) / 2));
					area.setText(df.format(total));
					break;
				case '6':
					num1 = Double.valueOf(base.getText());
					num2 = Double.valueOf(altura.getText());
					total = (num1 * num2) / 2;
					area.setText(df.format(total));
					break;
				default:
					JOptionPane.showMessageDialog(frmAreas,
							"Selecciones almenos una figura y rellene los campos",
							"Alert",
							JOptionPane.ERROR_MESSAGE);
					break;
				}
			}
		});
		salir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int respuesta = JOptionPane.showConfirmDialog(frmAreas, "�Realmente quieres salir?",
						"Confirmar por favor", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
				if (respuesta == 0) {
					frmAreas.dispose();
				}
			}
		});
	}

	public void limpiarLabels() {
		base.setBorder(new LineBorder(Color.BLACK));
		altura.setBorder(new LineBorder(Color.BLACK));
		lado.setBorder(new LineBorder(Color.BLACK));
		radio.setBorder(new LineBorder(Color.BLACK));
		bmayor.setBorder(new LineBorder(Color.BLACK));
		bmenor.setBorder(new LineBorder(Color.BLACK));
		dmayor.setBorder(new LineBorder(Color.BLACK));
		dmenor.setBorder(new LineBorder(Color.BLACK));

	}
}
