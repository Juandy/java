import java.util.InputMismatchException;
import java.util.Scanner;

public class Principal4 {

	public static void main(String[] args) {

		// Cada vez que estacionemos un vehículo, se pedirá la matrícula

		int n;
		int resultado = 0;

		do {

			System.out.println("+===========================+");
			System.out.println("|      Menu Aparcamiento    |");
			System.out.println("|---------------------------|");
			System.out.println("|1. Aparcar Coche(5€)       |");
			System.out.println("|---------------------------|");
			System.out.println("|2. Aparcar Moto(2€)        |");
			System.out.println("|---------------------_-----|");
			System.out.println("|3. Aparcar Camion(8€)      |");
			System.out.println("|---------------------------|");
			System.out.println("|4. Ver recaudación         |");
			System.out.println("|---------------------------|");
			System.out.println("|5. Salir                   |");
			System.out.println("+===========================+");

			System.out.print("Elija una opción: ");

			Scanner entrada = new Scanner(System.in);
			try {
				n = entrada.nextInt();
			} catch (InputMismatchException error) {
				n = 7;
			}

			System.out.println("\n");

			switch (n) {
			case 1:
				System.out.print("Introduce la matrícula del vehículo: ");
				String coche = entrada.next();
				Coche coche1 = new Coche("matricula", "Volvo", "S60", 25000, 'D', 123, 5, false);
				resultado += 5;
				break;
			case 2:
				System.out.print("Introduce la matrícula del vehículo: ");
				String moto = entrada.next();
				Moto moto1 = new Moto("matricula", "Honda", "Ninja", 50000, 250, 2, false);
				resultado += 2;
				break;
			case 3:
				System.out.print("Introduce la matrícula del vehículo: ");
				String camion = entrada.next();
				Camion camion1 = new Camion("matricula", "Daf", "730T", 85000, 5.5, 6, true, 2000, 'C');
				resultado += 8;
				break;
			case 4:
				System.out.print("la recaudacion total es: " + resultado + "\n");
				break;
			case 5:
				System.out.println("Ten un buen dia: ");
				break;
			default:
				System.out.println("El valor es INCORRECTO , Intentelo de nuevo (1-5)");
				break;
			}
		} while (n != 5);
	}
}
