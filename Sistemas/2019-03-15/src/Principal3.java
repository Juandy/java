
public class Principal3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Coche coche1 = new Coche("7092FFV", "Kia", "Niro", 20000, 'H', 204, 5, false);
		Moto moto1 = new Moto("9988HJC","Yamaha", "G35", 18000, 500, 4, false);
		Camion camion1 = new Camion("4126DRY", "Daf", "730T", 85000, 5.5, 6, true, 2000, 'C');
		
		Vehiculo vehiculo1 = new Coche("1234BXD", "Seat", "Leon", 15000, 'G', 223, 5, true);
		Vehiculo vehiculo2 = new Moto("8003HSX", "Honda", "CVR", 33000, 450, 4, true);
		Vehiculo vehiculo3 = new Camion("3266FVV", "Volvo", "FH16", 115000, 7.5, 6, false, 3500,'B');
		
		System.out.println(coche1);
		System.out.println(moto1);
		System.out.println(camion1);
	}
}
