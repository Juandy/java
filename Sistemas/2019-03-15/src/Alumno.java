
public class Alumno {
	
	// alumno -> DNI, Nombre, Apellidos, Altura, Edad, Sexo, Aprobado.
	
	String dni;
	String nombre;
	String apellido;
	double altura;
	int edad;
	char sexo;
	boolean aprobado;
	
	//Metodo constructor
	public Alumno(String dni, String nombre, String apellido, double altura, int edad, char sexo, boolean aprobado) {
		super();
		this.dni = dni;
		this.nombre = nombre;
		this.apellido = apellido;
		this.altura = altura;
		this.edad = edad;
		this.sexo = sexo;
		this.aprobado = aprobado;
	}

	//Métodos getters && setters
	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public double getAltura() {
		return altura;
	}

	public void setAltura(double altura) {
		this.altura = altura;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public char getSexo() {
		return sexo;
	}

	public void setSexo(char sexo) {
		this.sexo = sexo;
	}

	public boolean isAprobado() {
		return aprobado;
	}

	public void setAprobado(boolean aprobado) {
		this.aprobado = aprobado;
	}

	@Override
	public String toString() {
		return "Alumno [dni=" + dni + ", nombre=" + nombre + ", apellido=" + apellido + ", altura=" + altura + ", edad="
				+ edad + ", sexo=" + sexo + ", aprobado=" + aprobado + "]";
	}
	
}

