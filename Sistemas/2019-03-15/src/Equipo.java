
public class Equipo {
	
	//Equipo -> Nombre, plantilla, Fecha de Creacion, Presupuesto, estadio

	String nombre;
	int plantilla;
	int anios;
	double presupuesto;
	boolean estadio;
	char tipo; //F -> futbol, B -> Baloncesto

	
	//Metodo constructor
	
	public Equipo(String nombre, int jugadores, int anios, double presupuesto, boolean estadio, char tipo) {
		super();
		this.nombre = nombre;
		this.plantilla = jugadores;
		this.anios = anios;
		this.presupuesto = presupuesto;
		this.estadio = estadio;
		this.tipo = tipo;
	}

	//Métodos getters && setters
	
	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public int getPlantilla() {
		return plantilla;
	}


	public void setPlantilla(int plantilla) {
		this.plantilla = plantilla;
	}


	public int getFechaCreacion() {
		return anios;
	}


	public void setFechaCreacion(int anios) {
		this.anios = anios;
	}


	public double getPresupuesto() {
		return presupuesto;
	}


	public void setPresupuesto(double presupuesto) {
		this.presupuesto = presupuesto;
	}


	public boolean isEstadio() {
		return estadio;
	}


	public void setEstadio(boolean estadio) {
		this.estadio = estadio;
	}
	
	public char getTipo() {
		return tipo;
	}

	public void setTipo(char tipo) {
		this.tipo = tipo;
	}
	//Método TOSTRINGss
	@Override
	public String toString() {
		return "Equipo [nombre=" + nombre + ", plantilla=" + plantilla + ", años=" + anios + ", presupuesto="
				+ presupuesto + ", estadio=" + estadio + ", tipo=" + tipo + "]";
	}
	



	

	
	
	

	
}
