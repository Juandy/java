
public class Conejo extends Animal{
	
	//tamaño, pelo, campero
	
	//Atributos para la clase conejo
	double tamanio;
	char pelo; // 'R' rizado, 'L' liso, 
	boolean campero;
	
	// Método constructor para crear objetos Conejo
	public Conejo(String nombre, int edad, boolean chip, double peso, String color, double tamanio, char pelo,
			boolean campero) {
		super(nombre, edad, chip, peso, color);
		this.tamanio = tamanio;
		this.pelo = pelo;
		this.campero = campero;
	}
	// Método Getters && Setters
	public double getTamanio() {
		return tamanio;
	}

	public void setTamanio(double tamanio) {
		this.tamanio = tamanio;
	}

	public char getPelo() {
		return pelo;
	}

	public void setPelo(char pelo) {
		this.pelo = pelo;
	}

	public boolean isCampero() {
		return campero;
	}

	public void setCampero(boolean campero) {
		this.campero = campero;
	}
	
	//Método ToString
	@Override
	public String toString() {
		return "Conejo [nombre=" + nombre + ", edad=" + edad + ", chip=" + chip + ", peso=" + peso + ", color=" + color
				+ ", tamaño=" + tamanio + ", pelo=" + pelo + ", campero=" + campero + "]";
	}


	

}
