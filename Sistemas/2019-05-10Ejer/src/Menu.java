import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Toolkit;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import com.toedter.calendar.JDateChooser;
import javax.swing.JCheckBox;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.File;
import java.awt.event.ActionEvent;
import javax.swing.JSpinner;
import javax.swing.JScrollBar;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.AbstractListModel;
import javax.swing.JScrollPane;
import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.ImageIcon;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.SwingConstants;


public class Menu {

	private JFrame frmCheckin;
	private JTextField campoNombre;
	private JTextField campoApellido;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private final ButtonGroup buttonGroup_1 = new ButtonGroup();
	private JTextField campoOrigen;
	private JTextField campoDestino;
	private JLabel nombre;
	private JLabel apellido;
	private JLabel lblIda;
	private JLabel lblVuelta;
	//File archivo = new File ("archivo.txt");

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Menu window = new Menu();
					window.frmCheckin.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Menu() {
		initialize();
	}
	

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmCheckin = new JFrame();
		frmCheckin.setIconImage(Toolkit.getDefaultToolkit().getImage("IMG\\icon.png"));
		frmCheckin.setTitle("Check-In");
		frmCheckin.setBounds(100, 100, 965, 651);
		frmCheckin.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmCheckin.getContentPane().setLayout(null);
		
		nombre = new JLabel("Nombre ");
		nombre.setBounds(35, 26, 61, 25);
		frmCheckin.getContentPane().add(nombre);
		
		apellido = new JLabel("Apellidos");
		apellido.setBounds(35, 60, 61, 25);
		frmCheckin.getContentPane().add(apellido);
		
		campoNombre = new JTextField();
		campoNombre.setBorder(new LineBorder(new Color(0, 0, 0)));
		campoNombre.setBounds(106, 28, 258, 20);
		frmCheckin.getContentPane().add(campoNombre);
		campoNombre.setColumns(10);
		
		campoApellido = new JTextField();
		campoApellido.setBorder(new LineBorder(new Color(0, 0, 0)));
		campoApellido.setColumns(10);
		campoApellido.setBounds(106, 62, 258, 20);
		frmCheckin.getContentPane().add(campoApellido);
		
		JRadioButton rdbtnHombre = new JRadioButton("Hombre");
		rdbtnHombre.setContentAreaFilled(false);
		buttonGroup.add(rdbtnHombre);
		rdbtnHombre.setBounds(35, 107, 86, 23);
		frmCheckin.getContentPane().add(rdbtnHombre);
		
		JRadioButton rdbtnMujer = new JRadioButton("Mujer");
		rdbtnMujer.setContentAreaFilled(false);
		buttonGroup.add(rdbtnMujer);
		rdbtnMujer.setBounds(35, 133, 86, 23);
		frmCheckin.getContentPane().add(rdbtnMujer);
		
		JLabel lblAdultos = new JLabel("Adultos");
		lblAdultos.setBounds(396, 26, 61, 25);
		frmCheckin.getContentPane().add(lblAdultos);
		
		JLabel lblNios = new JLabel("Ni\u00F1os");
		lblNios.setBorder(null);
		lblNios.setBounds(505, 26, 61, 25);
		frmCheckin.getContentPane().add(lblNios);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setBorder(new LineBorder(new Color(0, 0, 0)));
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"0","1", "2", "3", "4", "5", "6", "7", "8", "9", "10"}));
		comboBox.setBounds(446, 28, 49, 20);
		frmCheckin.getContentPane().add(comboBox);
		
		JComboBox comboBox_1 = new JComboBox();
		comboBox_1.setModel(new DefaultComboBoxModel(new String[] {"0","1", "2", "3", "4", "5", "6", "7", "8", "9", "10"}));
		comboBox_1.setBounds(539, 28, 49, 20);
		frmCheckin.getContentPane().add(comboBox_1);
		
		lblIda = new JLabel("Ida");
		lblIda.setBounds(396, 65, 61, 25);
		frmCheckin.getContentPane().add(lblIda);
		
		JDateChooser ida = new JDateChooser();
		ida.setBorder(null);
		ida.setBounds(427, 65, 95, 20);
		frmCheckin.getContentPane().add(ida);
		
		lblVuelta = new JLabel("Vuelta");
		lblVuelta.setBounds(527, 65, 61, 25);
		frmCheckin.getContentPane().add(lblVuelta);
		
		JDateChooser vuelta = new JDateChooser();
		vuelta.setBorder(null);
		vuelta.setBounds(568, 65, 95, 20);
		frmCheckin.getContentPane().add(vuelta);
		
		JLabel lblAsiento = new JLabel("Asiento");
		lblAsiento.setBounds(598, 26, 61, 25);
		frmCheckin.getContentPane().add(lblAsiento);
		
		JComboBox clase = new JComboBox();
		clase.setBorder(new LineBorder(new Color(0, 0, 0)));
		clase.setModel(new DefaultComboBoxModel(new String[] {"Turista", "Bussines", "Primera"}));
		clase.setBounds(648, 27, 103, 20);
		frmCheckin.getContentPane().add(clase);
		
		JCheckBox terminos = new JCheckBox("Terminos y condiciones");
		terminos.setContentAreaFilled(false);
		terminos.setBounds(666, 516, 146, 23);
		frmCheckin.getContentPane().add(terminos);
		
		JButton aceptar = new JButton("Aceptar");
		aceptar.setEnabled(false);
		aceptar.setBorder(new LineBorder(new Color(0, 0, 0)));
		aceptar.setBounds(723, 541, 89, 23);
		frmCheckin.getContentPane().add(aceptar);
		
		JLabel lblMenu = new JLabel("Menu");
		lblMenu.setBounds(35, 170, 61, 25);
		frmCheckin.getContentPane().add(lblMenu);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(35, 193, 103, 77);
		frmCheckin.getContentPane().add(scrollPane);
		
		JList list = new JList();
		list.setSelectionBackground(new Color(154, 205, 50));
		list.setBorder(new LineBorder(new Color(0, 0, 0)));
		scrollPane.setViewportView(list);
		list.setVisibleRowCount(4);
		list.setModel(new AbstractListModel() {
			String[] values = new String[] {"Deluxe", "Classic", "Traditional", "Cheap", "Kids", "Vegan"};
			public int getSize() {
				return values.length;
			}
			public Object getElementAt(int index) {
				return values[index];
			}
		});
		list.setSelectedIndex(1);
		
		JLabel origen = new JLabel("Origen");
		origen.setBounds(303, 106, 61, 25);
		frmCheckin.getContentPane().add(origen);
		
		JLabel Destino = new JLabel("Destino");
		Destino.setBounds(303, 132, 61, 25);
		frmCheckin.getContentPane().add(Destino);
		
		JRadioButton IdaVuelta = new JRadioButton("Ida y vuelta");
		IdaVuelta.setContentAreaFilled(false);
		buttonGroup_1.add(IdaVuelta);
		IdaVuelta.setBounds(139, 107, 109, 23);
		frmCheckin.getContentPane().add(IdaVuelta);
		
		JRadioButton Ida = new JRadioButton("Ida");
		Ida.setContentAreaFilled(false);
		buttonGroup_1.add(Ida);
		Ida.setBounds(139, 133, 109, 23);
		frmCheckin.getContentPane().add(Ida);
		
		campoOrigen = new JTextField();
		campoOrigen.setColumns(10);
		campoOrigen.setBorder(new LineBorder(new Color(0, 0, 0)));
		campoOrigen.setBounds(351, 108, 258, 20);
		frmCheckin.getContentPane().add(campoOrigen);
		
		campoDestino = new JTextField();
		campoDestino.setColumns(10);
		campoDestino.setBorder(new LineBorder(new Color(0, 0, 0)));
		campoDestino.setBounds(351, 134, 258, 20);
		frmCheckin.getContentPane().add(campoDestino);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon("IMG\\fondo.jpg"));
		lblNewLabel.setBounds(0, 0, 949, 612);
		frmCheckin.getContentPane().add(lblNewLabel);
		
		//Eventos
		terminos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int respuesta = JOptionPane.showConfirmDialog(
						frmCheckin, 
						"Aceptas los terminos y conciones",
						"Confirmar por favor",
						JOptionPane.YES_NO_OPTION,
						JOptionPane.QUESTION_MESSAGE );
				
				if(terminos.isSelected()) {
					aceptar.setEnabled(true);
				}
				else {
					aceptar.setEnabled(false);
				}
				if(respuesta == 0) {
					terminos.setSelected(true);
					aceptar.setEnabled(true);
				}
				if(respuesta == 1) {
					terminos.setSelected(false);
					aceptar.setEnabled(false);
				}
			}
		});
		
		campoNombre.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {		
				limpiarLabels();
				nombre.setForeground(new Color(0, 128, 0));
			}
		});
		
		campoApellido.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				limpiarLabels();
				apellido.setForeground(new Color(0, 128, 0));
			}
		});
		
		
	}
	
	public void limpiarLabels () {
		nombre.setForeground(Color.BLACK);
		apellido.setForeground(Color.BLACK);
		lblIda.setForeground(Color.BLACK);
		lblVuelta.setForeground(Color.BLACK);
	}
}
