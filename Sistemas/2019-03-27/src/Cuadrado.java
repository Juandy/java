
public class Cuadrado extends Poligonos {
	// lado.

	// Atributos de la clase Cudrado
	private double lado;

	// Métodos de la clase Cuadrado
	public Cuadrado(String color, char tamanio, double lado) {
		super(color, tamanio);
		this.lado = lado;
	}

	// Métodos getters && setters
	public double getLado() {
		return lado;
	}

	public void setLado(double lado) {
		this.lado = lado;
	}

	// Método ToString
	@Override
	public String toString() {
		return "Cuadrado [color=" + color + ", tamanio=" + tamanio + ", lado=" + lado + "]";
	}

	// Calcular Area

	public double calcularArea() {
		double area;
		area = Math.pow(getLado(), 2);
		return area;

	}

}
