import java.util.InputMismatchException;
import java.util.Scanner;

public class Geometria {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int numero;

		do {

			// Creacion del menu
			System.out.println("*********************");
			System.out.println("*  Elija una opcion *");
			System.out.println("*********************");
			System.out.println("1. Circulo");
			System.out.println("2. Cuadrado");
			System.out.println("3. Tríangulo");
			System.out.println("4. Rombo");
			System.out.println("5. Trapecio");
			System.out.println("6. Rectángulo");
			System.out.println("7. Salir");
			System.out.println("\n");
			System.out.println("Elija una opcion");

			// Pedir los datos al usuario
			Scanner entrada = new Scanner(System.in);
			try {
			numero = entrada.nextInt();
			}catch(InputMismatchException error) {
				numero = 8;
			}

			System.out.println("\n");

			switch (numero) {
			case (1):
				System.out.println("Calcular area del circulo");
				System.out.print("Introduce su radio: ");
				double radio = 0;
				try {
				radio = entrada.nextDouble();
				}catch(InputMismatchException error) {
					System.out.println("Introduzca un valor correcto para el lado");
				}
				Circulo circulo1 = new Circulo("Rojo", 'P', radio);
				System.out.println("El area del circulo es " + circulo1.calcularArea());
				break;
			case (2):
				System.out.println("Calcular area de un cuadrado");
				System.out.print("Introduce su lado: ");
				double lado = 0;
				try {
				lado = entrada.nextDouble();
				}catch(InputMismatchException error) {
					System.out.println("Introduzca un valor correcto para el lado");
				}
				Cuadrado cuadrado1 = new Cuadrado("Verde", 'G', lado);
				System.out.println("El area del circulo es " + cuadrado1.calcularArea());
				System.out.println("\n");
				break;
			case (3):
				System.out.println("Calcular area de un tríangulo");
				System.out.println("Introduce su base:");
				double base = 0;
				try {
				base = entrada.nextDouble();
				}catch(InputMismatchException error) {
					System.out.println("Introduzca un valor correcto para el lado");
				}
				System.out.println("Introduce su altura:");
				double altura = entrada.nextDouble();
				Triangulo triangulo1 = new Triangulo("Azul", 'P', base, altura);
				System.out.println("el area del tríangulo es: " + triangulo1.calcularArea());
				System.out.println("\n");
				break;
			case (4):
				System.out.println("Calcular area de un rombo");
				System.out.println("Introduce su diagonal mayor:");
				double dMayor = entrada.nextDouble();
				System.out.println("Introduce su diagonal menor:");
				double dMenor = entrada.nextDouble();
				Rombo rombo1 = new Rombo("Azul", 'P', dMayor, dMenor);
				System.out.println("el area del rombo es: " + rombo1.calcularArea());
				System.out.println("\n");
				break;
			case (5):
				System.out.println("Calcular area de un trapecio");
				System.out.println("Introduce su base mayor:");
				double bMayor = entrada.nextDouble();
				System.out.println("Introduce su base menor:");
				double bMenor = entrada.nextDouble();
				System.out.println("Introduce su altura:");
				double trapecioaltura = entrada.nextDouble();
				Trapecio trapecio1 = new Trapecio("Azul", 'P', bMayor, bMenor, trapecioaltura);
				System.out.println("el area del rombo es: " + trapecio1.calcularArea());
				System.out.println("\n");
				break;
			case (6):
				System.out.println("Calcular area de un rectángulo");
				System.out.println("Introduce su base:");
				double Rbase = entrada.nextDouble();
				System.out.println("Introduce su altura:");
				double Raltura = entrada.nextDouble();
				Rectangulo rectangulo1 = new Rectangulo("Azul", 'P', Rbase, Raltura);
				System.out.println("el area del rombo es: " + rectangulo1.calcularArea());
				System.out.println("\n");
				break;
			case (7):
				System.out.println("Hasta luego");
				break;
			default:
				System.out.println("El numero introducido no es correcto, intentelo de nuevo (1-7)");
				break;
			}
		} while (numero != 7);
	}

}
