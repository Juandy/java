
public class Rombo extends Poligonos {

	// dMayor, dMenor.

	// Atributos para la clase Rombo
	private double dMayor;
	private double dMenor;

	// Método constructor para un Rombo
	public Rombo(String color, char tamanio, double dMayor, double dMenor) {
		super(color, tamanio);
		this.dMayor = dMayor;
		this.dMenor = dMenor;
	}

	// Método getters && setters
	public double getdMayor() {
		return dMayor;
	}

	public void setdMayor(double dMayor) {
		this.dMayor = dMayor;
	}

	public double getdMenor() {
		return dMenor;
	}

	public void setdMenor(double dMenor) {
		this.dMenor = dMenor;
	}

	// Método ToString
	@Override
	public String toString() {
		return "Rombo [color=" + color + ", tamanio=" + tamanio + ", dMayor=" + dMayor + ", dMenor=" + dMenor + "]";
	}

	// Calcular Area
	public double calcularArea() {
		double area;
		area = (getdMayor() * getdMenor()) / 2;
		return area;
	}

}
