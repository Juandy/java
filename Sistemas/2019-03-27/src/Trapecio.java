
public class Trapecio extends Poligonos {

	// bMayor, bMenor, altura.

	// Atributos de la clase Trapecio

	private double bMayor;
	private double bMenor;
	private double altura;

	// Método Constructor para el Trapecio
	public Trapecio(String color, char tamanio, double bMayor, double bMenor, double altura) {
		super(color, tamanio);
		this.bMayor = bMayor;
		this.bMenor = bMenor;
		this.altura = altura;
	}

	// Método getters && setters
	public double getbMayor() {
		return bMayor;
	}

	public void setdMayor(double bMayor) {
		this.bMayor = bMayor;
	}

	public double getbMenor() {
		return bMenor;
	}

	public void setdMenor(double dMenor) {
		this.bMenor = dMenor;
	}

	public double getAltura() {
		return altura;
	}

	public void setAltura(double altura) {
		this.altura = altura;
	}

	// Método ToString
	@Override
	public String toString() {
		return "Trapecio [color=" + color + ", tamanio=" + tamanio + ", dMayor=" + bMayor + ", dMenor=" + bMenor
				+ ", altura=" + altura + "]";
	}

	// Calcular Area

	public double calcularArea() {
		double area;
		area = (getAltura() * ((getbMayor() + getbMenor()) / 2));
		return area;
	}

}
