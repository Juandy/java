
public class Triangulo extends Poligonos {
	// Base, Altura.

	// Atributos de la clase Triangulo

	private double base;
	private double altura;

	// Método Constructor para el Triangulo
	public Triangulo(String color, char tamanio, double base, double altura) {
		super(color, tamanio);
		this.base = base;
		this.altura = altura;
	}

	// Métodos getters && setters
	public double getBase() {
		return base;
	}

	public void setBase(double base) {
		this.base = base;
	}

	public double getAltura() {
		return altura;
	}

	public void setAltura(double altura) {
		this.altura = altura;
	}

	// Método ToString
	@Override
	public String toString() {
		return "Triangulo [color=" + color + ", tamanio=" + tamanio + ", base=" + base + ", altura=" + altura + "]";
	}

	// Calcular Area
	public double calcularArea() {
		double area;
		area = (getBase() * getAltura()) / 2;
		return area;
	}

}
