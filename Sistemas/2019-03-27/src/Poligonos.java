
public class Poligonos {
	
	//Color, Tamanio.
	
	//Atributos para poligonos
	protected String color;
	protected char tamanio;//'G' Grande 'P' Pequenio
	
	//Metodo contructor Para la clase padre
	public Poligonos(String color, char tamanio) {
		super();
		this.color = color;
		this.tamanio = tamanio;
	}

	// Métodos getters && setters
	protected String getColor() {
		return color;
	}

	protected void setColor(String color) {
		this.color = color;
	}

	protected char getTamanio() {
		return tamanio;
	}

	protected void setTamanio(char tamanio) {
		this.tamanio = tamanio;
	}

	//Método ToString
	@Override
	public String toString() {
		return "Poligonos [color=" + color + ", tamanio=" + tamanio + "]";
	}


}
