
public class Circulo extends Poligonos {
	
	//Color, Tamanio, Double.
	
	
	//Atributos para la clase Circulo
	private double radio;

	//Metodo contructor Para la clase Circulo
	public Circulo(String color, char tamanio, double radio) {
		super(color, tamanio);
		this.radio = radio;
	}

	//Métodos getters && setters
	public double getRadio() {
		return radio;
	}

	public void setRadio(double radio) {
		this.radio = radio;
	}

	//Método ToString
	@Override
	public String toString() {
		return "Circulo [color=" + color + ", tamanio=" + tamanio + ", radio=" + radio + "]";
	}
	
	//Calcular el Area
	
	 public double calcularArea() {
		double area;
		area = Math.PI * Math.pow(getRadio(),2);
		return area;
	}

	
}
