
public class Rectangulo extends Poligonos {

	// Base, Altura.

	// Atributos de la clase Rectángulo
	private double base;
	private double altura;

	// Método constructor para un Rectángulo
	public Rectangulo(String color, char tamanio, double base, double altura) {
		super(color, tamanio);
		this.base = base;
		this.altura = altura;
	}

	// Método getters && setters
	public double getBase() {
		return base;
	}

	public void setBase(double base) {
		this.base = base;
	}

	public double getAltura() {
		return altura;
	}

	public void setAltura(double altura) {
		this.altura = altura;
	}

	// Método ToString
	@Override
	public String toString() {
		return "Rectangulo [color=" + color + ", tamanio=" + tamanio + ", base=" + base + ", altura=" + altura + "]";
	}

	// Calcular Area

	public double calcularArea() {
		double area;
		area = getBase() * getAltura();
		return area;
	}

}
