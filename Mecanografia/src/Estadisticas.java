import java.awt.EventQueue;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JTextArea;

import sun.invoke.empty.Empty;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.awt.event.ActionEvent;

public class Estadisticas {

	private JFrame frmEstadisticas;

	String EstadisticasUser;

	public JFrame getFrmEstadisticas() {
		return frmEstadisticas;
	}

	public void setFrmEstadisticas(JFrame frmEstadisticas) {
		this.frmEstadisticas = frmEstadisticas;
	}

	MenuEleccion principal;

	public Estadisticas() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmEstadisticas = new JFrame();
		frmEstadisticas.setIconImage(Toolkit.getDefaultToolkit().getImage("IMG\\iconJava.png"));
		frmEstadisticas.setBounds(500, 150, 847, 615);
		frmEstadisticas.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmEstadisticas.getContentPane().setLayout(null);

		JTextArea textAreaEstadisticas = new JTextArea();
		textAreaEstadisticas.setEditable(false);
		textAreaEstadisticas.setText("");
		textAreaEstadisticas.setBounds(10, 11, 574, 590);
		frmEstadisticas.getContentPane().add(textAreaEstadisticas);

		JButton btnMostrar = new JButton("Mostrar estadisticas");
		btnMostrar.setBounds(602, 12, 219, 64);
		frmEstadisticas.getContentPane().add(btnMostrar);

		JButton btnBorrarEstadisticas = new JButton("Borrar estadisticas");
		btnBorrarEstadisticas.setBounds(602, 87, 219, 64);
		frmEstadisticas.getContentPane().add(btnBorrarEstadisticas);


		//Eventos
		btnMostrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				textAreaEstadisticas.setText("");
					File fichero = new File("Files\\estadisticas.txt");
					String resultado= "";

					try {
						BufferedReader leer = new BufferedReader(new FileReader(fichero));
						String linea = leer.readLine();
						while (linea != null) 
						{
							textAreaEstadisticas.append(linea+"\n");
							linea = leer.readLine();

						}

					} catch (Exception e) {
						// TODO: handle exception
					}
			}
		});
		btnBorrarEstadisticas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textAreaEstadisticas.setText("");
			}
		});
	}
}
